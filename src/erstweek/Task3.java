package erstweek;

import java.util.Scanner;

/*
         Напишите программу, которая получает два числа с плавающей точкой x и y в аргументах
         командной строки и выводит еввклидово расстояние от точки (x,y) до точки (0,0)
         Входные данные:
         i=7, j=5
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите координаты точки (i,j)");
        double i = scanner.nextDouble();
        double j = scanner.nextDouble();
        double res = Math.sqrt(Math.pow(i, 2) + j * j);

        System.out.println("Растояние равно :" + Math.pow(i * i + j * j, 0.5));
        System.out.println(Math.sqrt(i * i + j * j));
        System.out.println("Результат работы программы :" + res);


    }
}
