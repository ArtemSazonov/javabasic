package erstweek;

import java.util.Scanner;

/*
Даны целые числа a, b, c, определяющие квадратное уравнение. Вычислить дискриминант.
a=6 b=-28 c=79
 */
public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        double d = Math.pow(b, 2) - 4 * a * c;
        System.out.println("Дискриминант равен :" + d);
    }
}
