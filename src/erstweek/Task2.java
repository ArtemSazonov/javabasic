package erstweek;

import java.util.Scanner;

/*
      Дано m - количество трафика, используемое пользователем за месяц,
      с - заплаченная цена за этот трафик.
      Вычислить стоимость одного гигабайта трафика.

      Входные данные:
      1) m=2, c=22
      2) m=3, c=22
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int c = scanner.nextInt();
        System.out.println("Стоимость одного Гб трафика равна " + c * 1.0 / m);
        System.out.println("Стоимость одного Гб трафика равна " + (double) c / m);
        System.out.println("Стоимость одного Гб трафика равна " + c * 1D / m);
    }
}
