package erstweek;

import java.util.Scanner;

/*
Дана площадь круга, нужно найти диаметр окружности и длину окружности

Входные данные:
Площадь равна 91
 */
public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double s = scanner.nextDouble();
        System.out.println("Диаметр окружности равен:" + Math.sqrt(4 * s / Math.PI) +
                " Длина окружности равна:" + Math.sqrt(s * 4 * Math.PI));
    }
}
