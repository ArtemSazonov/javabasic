package secondweek;

import java.util.Scanner;

/*
Реализовать System.out.println(), используя System.out.print() и табуляцию \n
Входные данные: два слова, считываемые из консоли
Входные данные
Hello World
Выходные данные
Hello
World
 */
public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String ferst = scanner.next();
        String second = scanner.next();
        System.out.print(ferst + "\n" + second);
    }
}
