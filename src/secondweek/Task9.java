package secondweek;

import java.util.Scanner;

/*
Дана строка и паттерн, заменить паттерн на паттерн, состоящий из заглавных символов
Входные данные
Hello
o
Выходные данные
HellO
Входные данные
Hello world
ld
Выходные данные
Hello worLD
 */
public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String pattern = scanner.next();

        String upperPattern = pattern.toUpperCase();
        System.out.println(str.replace(pattern,upperPattern));
    }
}
