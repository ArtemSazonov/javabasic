package secondweek;

import java.util.Scanner;

/*
Даны три числа a, b, c.
Найти сумму двух чисел больших из них.
входные данные
21 0 8
Выходные данные
29
 */
public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        if (a > c && b > c) {
            System.out.println(a + b);
        } else if (a > b && c > b) {
            System.out.println(a + c);
        } else if (b > a && c > a) {
            System.out.println(b + c);
        }
        System.out.println(Math.max(Math.max(a + b, b + c), a + c));// Альтернатива
    }
}
