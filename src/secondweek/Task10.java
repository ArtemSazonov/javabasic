package secondweek;

import java.util.Scanner;

/*
Дана последовательность символов, конкатинировать их в одну строку и вывести эту строку, исключая цифры.
На вход подаются заглавные или строчные символы английского алфавита или цифры.
Входные данные
H 1 9 i 4
Выходные данные
HI
 */
public class Task10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        char a1 = input.next().charAt(0);
        char a2 = input.next().charAt(0);
        char a3 = input.next().charAt(0);
        char a4 = input.next().charAt(0);
        char a5 = input.next().charAt(0);

        String res = "";
        if ((a1 >= 'a' && a1 <= 'z') || (a1 >= 'A' && a1 <= 'Z')) {
            res += a1;
        }
        if ((a2 >= 'a' && a2 <= 'z') || (a2 >= 'A' && a2 <= 'Z')) {
            res += a2;
        }
        if ((a3 >= 'a' && a3 <= 'z') || (a3 >= 'A' && a3 <= 'Z')) {
            res += a3;
        }
        if ((a4 >= 'a' && a4 <= 'z') || (a4 >= 'A' && a4 <= 'Z')) {
            res += a4;
        }
        if ((a5 >= 'a' && a5 <= 'z') || (a5 >= 'A' && a5 <= 'Z')) {
            res += a1;
        }
        System.out.println(res);

    }

    public static String checkString(char input) {
        String res = "";

        if ((input >= 'a' && input <= 'z') || (input >= 'A' && input <= 'Z')) {
            res += input;
        }
        return res;
    }
}
