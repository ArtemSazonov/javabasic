package secondweek;
/*
Считать данные из консоли о типе номера отеля.
1 VIP, 2 Premium, 3 Standard
Вывести цену номера VIP = 125, Premium = 110, Standard = 100
 */

import java.util.Scanner;

public class Task4 {
    public static final int VIP_PRISE = 125;
    public static final int PREMIUM_PRISE = 110;
    public static final int STANDARD_PRISE = 100;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int roomType = scanner.nextInt();
        if (roomType == 1) {
            System.out.println("VIP Price: " + VIP_PRISE);
        }else if (roomType == 2) {
            System.out.println("Premium Price: " + PREMIUM_PRISE);
        }else if (roomType == 3) {
            System.out.println("Standard Price: " + STANDARD_PRISE);
        }
        else {
            System.out.println("Введите корректный номер");
        }
        switch (roomType) {
            case 1:
                System.out.println("VIP Price: " + VIP_PRISE);
                break;
                case 2:
                System.out.println("Premium Price: " + PREMIUM_PRISE);
                break;
                case 3:
                System.out.println("Standard Price: " + STANDARD_PRISE);
                break;
            default:
                System.out.println("Введите корректный номер");
                break;

        }

    }
}
